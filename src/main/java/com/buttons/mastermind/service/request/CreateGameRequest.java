package com.buttons.mastermind.service.request;

public class CreateGameRequest {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
