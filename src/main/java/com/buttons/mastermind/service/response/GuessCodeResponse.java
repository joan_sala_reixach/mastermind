package com.buttons.mastermind.service.response;

public class GuessCodeResponse {

    private final String message;

    public GuessCodeResponse(String feedback) {
        if (feedback == null) {
            this.message = "Something went wrong!";
        } else {
            this.message = "You got the following result: " + feedback;
        }
    }

    public String getMessage() {
        return message;
    }
}
