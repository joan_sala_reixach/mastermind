package com.buttons.mastermind.service.response;

public class CreateGameResponse {

    private final String message;

    public CreateGameResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
