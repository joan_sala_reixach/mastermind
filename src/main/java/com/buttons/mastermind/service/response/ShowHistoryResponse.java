package com.buttons.mastermind.service.response;

public class ShowHistoryResponse {

    private final String message;

    public ShowHistoryResponse(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
