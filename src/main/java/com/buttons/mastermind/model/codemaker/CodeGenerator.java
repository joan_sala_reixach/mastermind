package com.buttons.mastermind.model.codemaker;

import com.buttons.mastermind.model.board.GameCode;
import com.buttons.mastermind.model.board.GameConstants;
import com.buttons.mastermind.model.board.CodePeg;
import com.buttons.mastermind.model.board.PegColor;
import com.buttons.mastermind.model.exception.GameException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class CodeGenerator {

    private static final List<PegColor> VALUES = Collections.unmodifiableList(Arrays.asList(PegColor.values()));
    private static final Random RANDOM = new Random();

    public GameCode generateCode() throws GameException {
        CodePeg[] codePegs = new CodePeg[GameConstants.CODE_LENGTH];
        for (int i = 0; i < GameConstants.CODE_LENGTH; i++) {
            int pick = RANDOM.nextInt(VALUES.size());
            codePegs[i] = new CodePeg(VALUES.get(pick));
        }
        return new GameCode(codePegs);
    }
}
