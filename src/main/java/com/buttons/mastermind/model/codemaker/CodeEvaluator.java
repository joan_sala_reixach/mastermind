package com.buttons.mastermind.model.codemaker;

import com.buttons.mastermind.model.board.GameCode;
import com.buttons.mastermind.model.board.GameConstants;
import com.buttons.mastermind.model.exception.FeedbackException;
import com.buttons.mastermind.model.feedback.FeedbackCode;
import com.buttons.mastermind.model.feedback.FeedbackColor;

import java.util.Arrays;

public class CodeEvaluator {

    private class EvaluatorHelper {
        private final FeedbackCode feedbackCode;
        private final boolean[] usedGuessPegs;
        private final boolean[] usedHiddenCodePegs;

        protected EvaluatorHelper() {
            feedbackCode = new FeedbackCode();
            usedGuessPegs = new boolean[4];
            usedHiddenCodePegs = new boolean[4];
            Arrays.fill(usedGuessPegs, false);
            Arrays.fill(usedHiddenCodePegs, false);
        }

        protected EvaluatorHelper(FeedbackCode feedbackCode, boolean[] usedGuessPegs, boolean[] usedHiddenCodePegs) {
            this.feedbackCode = feedbackCode;
            this.usedGuessPegs = usedGuessPegs;
            this.usedHiddenCodePegs = usedHiddenCodePegs;
        }

        protected EvaluatorHelper(EvaluatorHelper helper) {
            this.feedbackCode = helper.getFeedbackCode();
            this.usedGuessPegs = helper.getUsedGuessPegs();
            this.usedHiddenCodePegs = helper.getUsedHiddenCodePegs();
        }

        protected boolean isUsedGuessPeg(int position) {
            return usedGuessPegs[position];
        }

        protected boolean isUsedHiddenCodePeg(int position) {
            return usedHiddenCodePegs[position];
        }

        protected FeedbackCode getFeedbackCode() {
            return feedbackCode;
        }

        protected boolean[] getUsedGuessPegs() {
            return Arrays.copyOf(this.usedGuessPegs, GameConstants.CODE_LENGTH);
        }

        protected boolean[] getUsedHiddenCodePegs() {
            return Arrays.copyOf(this.usedHiddenCodePegs, GameConstants.CODE_LENGTH);
        }
    }

    public FeedbackCode evaluateCode(GameCode guessCode, GameCode hiddenCode) {
        EvaluatorHelper helper = new EvaluatorHelper();
        try {
            helper = checkPerfectMatches(helper, guessCode, hiddenCode);
            helper = checkColorMatches(helper, guessCode, hiddenCode);
        } catch (FeedbackException e) {
            // TODO handle exception as appropiate (log and throw)
        }
        return helper.getFeedbackCode();
    }

    private EvaluatorHelper checkPerfectMatches(EvaluatorHelper helper, GameCode guessCode, GameCode hiddenCode) throws FeedbackException {
        EvaluatorHelper resultHelper = new EvaluatorHelper(helper);
        for (int position = 0; position < GameConstants.CODE_LENGTH; position++) {
            if (guessCode.getPegs()[position].getPegColor().equals(hiddenCode.getPegs()[position].getPegColor())) {
                resultHelper = setFeedbackPeg(resultHelper, position, position, FeedbackColor.BLACK);
            }
        }
        return resultHelper;
    }

    private EvaluatorHelper checkColorMatches(EvaluatorHelper helper, GameCode guessCode, GameCode hiddenCode) throws FeedbackException {
        EvaluatorHelper resultHelper = new EvaluatorHelper(helper);
        for (int guessPosition = 0; guessPosition < GameConstants.CODE_LENGTH; guessPosition++) {
            for (int hiddenCodePosition = 0; hiddenCodePosition < GameConstants.CODE_LENGTH; hiddenCodePosition++) {
                if (!resultHelper.getUsedGuessPegs()[guessPosition]
                        && !resultHelper.getUsedHiddenCodePegs()[hiddenCodePosition]
                        && guessCode.getPegs()[guessPosition].getPegColor().equals(hiddenCode.getPegs()[hiddenCodePosition].getPegColor())) {
                    resultHelper = setFeedbackPeg(resultHelper, guessPosition, hiddenCodePosition, FeedbackColor.WHITE);
                }
            }
        }
        return resultHelper;
    }

    private EvaluatorHelper setFeedbackPeg(EvaluatorHelper helper, int guessPosition,
                                           int hiddenCodePosition, FeedbackColor feedbackColor) throws FeedbackException {
        boolean[] usedGuessPegs = helper.getUsedGuessPegs();
        boolean[] usedHiddenCodePegs = helper.getUsedHiddenCodePegs();
        FeedbackCode feedbackCode = helper.getFeedbackCode().addPeg(feedbackColor);
        usedGuessPegs[guessPosition] = true;
        usedHiddenCodePegs[hiddenCodePosition] = true;
        return new EvaluatorHelper(feedbackCode, usedGuessPegs, usedHiddenCodePegs);
    }
}
