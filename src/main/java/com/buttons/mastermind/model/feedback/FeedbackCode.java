package com.buttons.mastermind.model.feedback;

import com.buttons.mastermind.model.board.Code;
import com.buttons.mastermind.model.board.GameConstants;
import com.buttons.mastermind.model.board.Peg;
import com.buttons.mastermind.model.exception.FeedbackException;

import java.util.Arrays;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class FeedbackCode implements Code {

    private final Peg[] feedbackPegs;
    private final int index;

    public FeedbackCode() {
        this.feedbackPegs = new Peg[GameConstants.CODE_LENGTH];
        for (int i = 0; i < feedbackPegs.length; i++) {
            feedbackPegs[i] = new FeedbackPeg(FeedbackColor.NONE);
        }
        index = 0;
    }

    public FeedbackCode(final Peg[] feedbackPegs, final int index) {
        this.feedbackPegs = feedbackPegs;
        this.index = index;
    }

    public FeedbackCode(FeedbackCode feedbackCode) {
        this.feedbackPegs = feedbackCode.feedbackPegs;
        this.index = feedbackCode.index;
    }

    public FeedbackCode addPeg(com.buttons.mastermind.model.feedback.FeedbackColor color) throws FeedbackException {
        if (this.index < GameConstants.CODE_LENGTH) {
            Peg[] pegs = this.feedbackPegs.clone();
            pegs[index] = new com.buttons.mastermind.model.feedback.FeedbackPeg(color);
            return new FeedbackCode(pegs, index + 1);
        } else {
            throw new FeedbackException("Cannot add more pegs to this feedback");
        }
    }

    public Peg[] getPegs() {
        return feedbackPegs;
    }

    @Override
    public String toString() {
        return Arrays.stream(feedbackPegs).
                filter(peg -> !FeedbackColor.NONE.equals(peg.getPegColor())).
                map(Object::toString).
                collect(Collectors.joining(",")).toString();
    }
}
