package com.buttons.mastermind.model.feedback;

import com.buttons.mastermind.model.board.Color;

public enum FeedbackColor implements Color {
        BLACK, WHITE, NONE
}
