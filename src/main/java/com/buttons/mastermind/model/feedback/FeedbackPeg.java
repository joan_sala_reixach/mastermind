package com.buttons.mastermind.model.feedback;

import com.buttons.mastermind.model.board.Peg;

public class FeedbackPeg implements Peg {

    private final FeedbackColor feedbackColor;

    public FeedbackPeg(FeedbackColor feedbackColor) {
        this.feedbackColor = feedbackColor;
    }

    public FeedbackColor getPegColor() {
        return feedbackColor;
    }

    @Override
    public String toString() {
        return feedbackColor.toString();
    }
}

