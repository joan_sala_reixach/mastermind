package com.buttons.mastermind.model.exception;

public class FeedbackException extends Exception {

    public FeedbackException(String message) {
        super(message);
    }
}
