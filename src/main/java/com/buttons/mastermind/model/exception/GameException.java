package com.buttons.mastermind.model.exception;

public class GameException extends Exception {

    public GameException(String message) {
        super(message);
    }
}
