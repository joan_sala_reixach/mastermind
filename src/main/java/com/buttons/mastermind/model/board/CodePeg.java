package com.buttons.mastermind.model.board;

public class CodePeg implements Peg {
    private final PegColor pegColor;

    public CodePeg(PegColor pegColor) {
        this.pegColor = pegColor;
    }

    public PegColor getPegColor() {
        return pegColor;
    }

    @Override
    public String toString() {
        return pegColor.toString();
    }
}
