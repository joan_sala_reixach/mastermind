package com.buttons.mastermind.model.board;

import com.buttons.mastermind.model.codemaker.CodeEvaluator;
import com.buttons.mastermind.model.codemaker.CodeGenerator;
import com.buttons.mastermind.model.exception.GameException;
import com.buttons.mastermind.model.feedback.FeedbackCode;
import com.buttons.mastermind.service.request.GuessCodeRequest;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class BoardManager {

    private Game game = null;

    private Map<GameCode, FeedbackCode> guessHistory;

    private static BoardManager boardManager = null;

    private BoardManager() {
        guessHistory = new HashMap<>();
    }

    public synchronized static BoardManager getBoardManager() {
        if (boardManager == null) {
            boardManager = new BoardManager();
        }
        return boardManager;
    }

    public void startGame() throws GameException {
        if (game == null) {
            GameCode hiddenCode = new CodeGenerator().generateCode();
            game = new Game(hiddenCode);
        } else {
            throw new GameException("Game already started! Finish the game before starting a new one!");
        }
    }

    public String evaluateGuess(GuessCodeRequest guessCodeRequest) {
        try {
            GameCode guessCode = retrieveGuessCode(guessCodeRequest);
            CodeEvaluator evaluator = new CodeEvaluator();
            FeedbackCode feedbackCode = evaluator.evaluateCode(guessCode, game.getHiddenCode());
            guessHistory.put(guessCode, feedbackCode);
            return feedbackCode.toString();
        } catch (GameException e) {
            return e.getMessage();
        }
    }

    public String showHistory() {
        String formattedHistory = guessHistory.entrySet()
                .stream()
                .map(entry -> entry.getKey() + " - " + entry.getValue())
                .collect(Collectors.joining(", "));
        return formattedHistory;
    }

    public Game getGame() {
        return game;
    }

    private GameCode retrieveGuessCode(GuessCodeRequest guessCodeRequest) throws GameException {
        Peg[] guessPegs = new Peg[GameConstants.CODE_LENGTH];
        PegColorBuilder builder = new PegColorBuilder();
        guessPegs[0] = new CodePeg(builder.getPegColor(guessCodeRequest.getColor0()));
        guessPegs[1] = new CodePeg(builder.getPegColor(guessCodeRequest.getColor1()));
        guessPegs[2] = new CodePeg(builder.getPegColor(guessCodeRequest.getColor2()));
        guessPegs[3] = new CodePeg(builder.getPegColor(guessCodeRequest.getColor3()));
        return new GameCode(guessPegs);
    }
}
