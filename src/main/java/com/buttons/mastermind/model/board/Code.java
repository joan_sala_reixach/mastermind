package com.buttons.mastermind.model.board;

public interface Code {

    Peg[] getPegs();
}
