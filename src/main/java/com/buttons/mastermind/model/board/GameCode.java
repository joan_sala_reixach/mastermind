package com.buttons.mastermind.model.board;

import com.buttons.mastermind.model.exception.GameException;

import java.util.Arrays;
import java.util.stream.Collectors;

public class GameCode implements Code {
    private final Peg[] codePegs;

    public GameCode(final Peg[] codePegs) throws GameException {
        if (codePegs != null && codePegs.length == 4) {
            this.codePegs = codePegs;
        } else {
            // this code is invalid
            throw new GameException("Cannot create game, invalid pegs");
        }
    }

    public Peg[] getPegs() {
        return codePegs;
    }

    @Override
    public String toString() {
        return Arrays.stream(codePegs).
                map(Object::toString).
                collect(Collectors.joining(",")).toString();
    }
}
