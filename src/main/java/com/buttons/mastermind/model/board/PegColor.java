package com.buttons.mastermind.model.board;

public enum PegColor implements Color {
        RED, BLUE, GREEN, YELLOW, PURPLE
}
