package com.buttons.mastermind.model.board;

import com.buttons.mastermind.model.exception.GameException;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PegColorBuilder {

    public PegColor getPegColor(String color) throws GameException {
        switch (color) {
            case "BLUE":
                return PegColor.BLUE;
            case "RED":
                return PegColor.RED;
            case "PURPLE":
                return PegColor.PURPLE;
            case "GREEN":
                return PegColor.GREEN;
            case "YELLOW":
                return PegColor.YELLOW;
            default:
                throw new GameException(color + " is not a valid peg color");
        }
    }
}
