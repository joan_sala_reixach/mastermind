package com.buttons.mastermind.model.board;

public interface Peg {

    Color getPegColor();
}
