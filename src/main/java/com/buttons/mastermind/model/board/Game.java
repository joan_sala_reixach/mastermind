package com.buttons.mastermind.model.board;

public class Game {

    private final GameCode hiddenCode;

    public Game(GameCode hiddenCode) {
        this.hiddenCode = hiddenCode;
    }

    public GameCode getHiddenCode() {
        return hiddenCode;
    }
}
