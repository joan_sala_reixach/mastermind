package com.buttons.application;

import com.buttons.mastermind.model.board.*;
import com.buttons.mastermind.model.exception.GameException;
import com.buttons.mastermind.model.feedback.FeedbackCode;
import com.buttons.mastermind.service.request.CreateGameRequest;
import com.buttons.mastermind.service.request.GuessCodeRequest;
import com.buttons.mastermind.service.response.CreateGameResponse;
import com.buttons.mastermind.service.response.GuessCodeResponse;
import com.buttons.mastermind.service.response.ShowHistoryResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@RestController
public class MastermindWebServiceController {

    @GetMapping("/healthcheck")
    public String welcome() {
        return "Welcome to mastermind! The application is up and running, please go to /help for usage instructions";
    }

    @GetMapping("/help")
    public String help() {
        return "To start a game, execute /createGame, to evaluate a guess execute /guess, use /showHistory to see the guess history";
    }

    @PostMapping("/createGame")
    public CreateGameResponse createGame(CreateGameRequest createGameRequest) {
        String message;
        try {
            BoardManager.getBoardManager().startGame();
            message = "Game Created Successfully, " + createGameRequest.getName() + "! For testing purposes," +
                    "the generated hidden code is: " + BoardManager.getBoardManager().getGame().getHiddenCode().toString();
        } catch (GameException e) {
            message = "Sorry " + createGameRequest.getName() + ", there has been a problem creating your game: " + e.getMessage();
        }
        return new CreateGameResponse(message);
    }

    @PostMapping("/guessCode")
    public GuessCodeResponse guessCode(GuessCodeRequest guessCodeRequest) {
        String feedback = BoardManager.getBoardManager().evaluateGuess(guessCodeRequest);
        return new GuessCodeResponse(feedback);
    }

    @PostMapping("/showHistory")
    public ShowHistoryResponse showHistory() {
        String formattedHistory = BoardManager.getBoardManager().showHistory();
        return new ShowHistoryResponse(formattedHistory);
    }
}
