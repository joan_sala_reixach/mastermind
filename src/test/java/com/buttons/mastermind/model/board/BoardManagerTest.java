package com.buttons.mastermind.model.board;

import com.buttons.mastermind.model.exception.GameException;
import org.junit.Assert;
import org.junit.Test;

public class BoardManagerTest {

    @Test
    public void testStartGame() throws GameException {
        BoardManager manager = BoardManager.getBoardManager();
        manager.startGame();
        Assert.assertNotNull(manager.getGame());
    }

    @Test(expected = GameException.class)
    public void testStartGameOnlyOnce() throws GameException {
        BoardManager manager = BoardManager.getBoardManager();
        manager.startGame();
        manager.startGame();
    }
}
