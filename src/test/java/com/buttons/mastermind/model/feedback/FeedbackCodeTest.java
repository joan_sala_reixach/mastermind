package com.buttons.mastermind.model.feedback;

import com.buttons.mastermind.util.MastermindTestUtils;
import org.junit.Test;

public class FeedbackCodeTest {

    @Test
    public void testNoneInit() {
        com.buttons.mastermind.model.feedback.FeedbackCode feedbackCode = new com.buttons.mastermind.model.feedback.FeedbackCode();
        MastermindTestUtils.checkFeedback(feedbackCode, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE);
    }

    @Test
    public void testAddBlack() throws Exception {
        com.buttons.mastermind.model.feedback.FeedbackCode feedbackCode = new com.buttons.mastermind.model.feedback.FeedbackCode().addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.BLACK);
        MastermindTestUtils.checkFeedback(feedbackCode, com.buttons.mastermind.model.feedback.FeedbackColor.BLACK, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE);
    }

    @Test
    public void testAddWhite() throws Exception {
        com.buttons.mastermind.model.feedback.FeedbackCode feedbackCode = new com.buttons.mastermind.model.feedback.FeedbackCode().addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.WHITE);
        MastermindTestUtils.checkFeedback(feedbackCode, com.buttons.mastermind.model.feedback.FeedbackColor.WHITE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE, com.buttons.mastermind.model.feedback.FeedbackColor.NONE);
    }

    @Test(expected = Exception.class)
    public void testFeedbackFull() throws Exception {
        com.buttons.mastermind.model.feedback.FeedbackCode feedbackCode = new com.buttons.mastermind.model.feedback.FeedbackCode()
                .addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.BLACK)
                .addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.BLACK)
                .addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.BLACK)
                .addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.BLACK)
                .addPeg(com.buttons.mastermind.model.feedback.FeedbackColor.BLACK);
    }
}
