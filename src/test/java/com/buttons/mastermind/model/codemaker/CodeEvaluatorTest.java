package com.buttons.mastermind.model.codemaker;

import com.buttons.mastermind.model.board.GameCode;
import com.buttons.mastermind.model.board.PegColor;
import com.buttons.mastermind.model.feedback.FeedbackCode;
import com.buttons.mastermind.model.feedback.FeedbackColor;
import com.buttons.mastermind.util.MastermindTestUtils;
import org.junit.Test;

public class CodeEvaluatorTest {

    private final GameCode hiddenCode = MastermindTestUtils
            .createTestCode(PegColor.BLUE, PegColor.GREEN, PegColor.RED, PegColor.PURPLE);
    private final CodeEvaluator codeEvaluator = new CodeEvaluator();

    @Test
    public void testFullMatch() {
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(hiddenCode, hiddenCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.BLACK, FeedbackColor.BLACK, FeedbackColor.BLACK, FeedbackColor.BLACK);
    }

    @Test
    public void testOnlyColorMatch() {
        GameCode guessCode = MastermindTestUtils
                .createTestCode(PegColor.RED, PegColor.PURPLE, PegColor.GREEN, PegColor.BLUE);
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(guessCode, hiddenCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.WHITE, FeedbackColor.WHITE, FeedbackColor.WHITE, FeedbackColor.WHITE);
    }

    @Test
    public void testNoMatch() {
        GameCode guessCode = MastermindTestUtils
                .createTestCode(PegColor.YELLOW, PegColor.YELLOW, PegColor.YELLOW, PegColor.YELLOW);
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(guessCode, hiddenCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.NONE, FeedbackColor.NONE, FeedbackColor.NONE, FeedbackColor.NONE);
    }

    @Test
    public void testMixedMatch() {
        GameCode guessCode = MastermindTestUtils
                .createTestCode(PegColor.BLUE, PegColor.RED, PegColor.PURPLE, PegColor.YELLOW);
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(guessCode, hiddenCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.BLACK, FeedbackColor.WHITE, FeedbackColor.WHITE, FeedbackColor.NONE);
    }

    @Test
    public void testRepeatedColors() {
        GameCode guessCode = MastermindTestUtils
                .createTestCode(PegColor.BLUE, PegColor.BLUE, PegColor.BLUE, PegColor.BLUE);
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(guessCode, guessCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.BLACK, FeedbackColor.BLACK, FeedbackColor.BLACK, FeedbackColor.BLACK);
    }

    @Test
    public void testDoubleScoring() {
        GameCode guessCode = MastermindTestUtils
                .createTestCode(PegColor.BLUE, PegColor.RED, PegColor.RED, PegColor.RED);
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(guessCode, hiddenCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.BLACK, FeedbackColor.BLACK, FeedbackColor.NONE, FeedbackColor.NONE);
    }

    @Test
    public void testDoubleScoringColorMatch() {
        GameCode guessCode = MastermindTestUtils
                .createTestCode(PegColor.GREEN, PegColor.YELLOW, PegColor.GREEN, PegColor.GREEN);
        FeedbackCode feedbackCode = codeEvaluator.evaluateCode(guessCode, hiddenCode);
        MastermindTestUtils.checkFeedback(feedbackCode, FeedbackColor.WHITE, FeedbackColor.NONE, FeedbackColor.NONE, FeedbackColor.NONE);
    }
}
