package com.buttons.mastermind.model.codemaker;

import com.buttons.mastermind.model.board.GameCode;
import com.buttons.mastermind.model.board.GameConstants;
import com.buttons.mastermind.model.exception.GameException;
import org.junit.Test;
import org.testng.Assert;

public class CodeGeneratorTest {

    private final CodeGenerator codeGenerator = new CodeGenerator();

    @Test
    public void testGenerateCode() throws GameException {
        GameCode code = codeGenerator.generateCode();
        Assert.assertTrue(code.getPegs().length == GameConstants.CODE_LENGTH);
    }
}
