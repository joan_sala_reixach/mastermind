package com.buttons.mastermind.util;

import com.buttons.mastermind.model.board.*;
import com.buttons.mastermind.model.exception.GameException;
import com.buttons.mastermind.model.feedback.FeedbackCode;
import com.buttons.mastermind.model.feedback.FeedbackColor;
import org.junit.Assert;

public class MastermindTestUtils {

    public static GameCode createTestCode(PegColor color0, PegColor color1, PegColor color2, PegColor color3) {
        try {
            CodePeg[] pegs = new CodePeg[GameConstants.CODE_LENGTH];
            pegs[0] = new CodePeg(color0);
            pegs[1] = new CodePeg(color1);
            pegs[2] = new CodePeg(color2);
            pegs[3] = new CodePeg(color3);
            return new GameCode(pegs);
        } catch (GameException e) {
            return null;
        }
    }

    public static void checkFeedback(FeedbackCode feedbackCode, FeedbackColor color0,
                                     FeedbackColor color1, FeedbackColor color2, FeedbackColor color3) {
        Assert.assertEquals(color0, feedbackCode.getPegs()[0].getPegColor());
        Assert.assertEquals(color1, feedbackCode.getPegs()[1].getPegColor());
        Assert.assertEquals(color2, feedbackCode.getPegs()[2].getPegColor());
        Assert.assertEquals(color3, feedbackCode.getPegs()[3].getPegColor());
    }
}
